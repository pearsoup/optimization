﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    private void OnTriggerEnter(Collider other)
    {
        //when the player collides with collectable, the collectable disappears
        
                Destroy(this.gameObject);

    }
}
