﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0)) {
            //suppose to be on click that the player follows the mouse's position
            Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                GetComponent<NavMeshAgent>().destination = hit.point;
            }
        }
  
	}
}
