﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        //when player clicks mouse, the balls will instantiate out a random colour from the position of where the mouse is
        if ( Input.GetButton( "Fire1" ) )
        {
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
            Destroy(prefabInstance, Random.Range(15f, 35f));
        }
    }

}
